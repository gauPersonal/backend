require('../../core/dbs/mongo'); // Required for connection
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	name: { type: String, required: true },
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	countryCode: { type: String, required: true },
	mobileNo: { type: String, required: true },
	createdAt: { type: Number, default: Date.now },
	updatedAt: { type: Number, default: Date.now },
});

userSchema.methods.toJSON = function () {
	const obj = this.toObject();
	delete obj.createdAt;
	delete obj.updatedAt;
	delete obj.__v;
	return obj;
};

module.exports = mongoose.model('user', userSchema);
