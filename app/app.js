"use strict";


require('newrelic');
const app = require('express')();
const exceptions = require('./core/handlers/exceptionHandler');
const middlewares = require('./core/middlewares');
const routes = require('./lib/routes');

/**
 * @description Middlewares
 */
middlewares.express(app);


/**
 * @description Added security
 */
app.use(middlewares.security);


/**
 * @description Injecting all Routes
 */
routes(app);


/**
 * @description Catch 404 error if no route found
 */
app.use(exceptions.unknownRouteHandler);


/**
 * @description This middleware will handle all the unexpected errors
 */
app.use(exceptions.errorHandler);

module.exports = app;
