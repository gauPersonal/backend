// External dependencies
const helmet = require('helmet');
const cors = require('cors')
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../../doc');

module.exports = function (app) {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  /**
  * @description Added xssFilter to prevent Cross-site scripting
  */
  app.use(helmet.xssFilter());

  /**
  * @description Added frameguard to mitigates clickjacking attacks
  */
  app.use(helmet.frameguard());


  /**
    * @description Added middleware for swagger
   */
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  /**
   * @description cors configuration
   * [TODO] add security options in cors 
  */
 app.use(cors())

};


