"use strict";

/**
 *@description Error Handler
 */
exports.errorHandler = (err, req, res, next) => {
  return res.status(err.status || 500).json({
    status: false,
    statusMessage: err.message,
    statusCode: err.statusCode || 'unexpectedError',
    response: err.response || {},
  });
};


exports.validationErrors = (err) => {
  const errors = this.parseValidationErrors(err);
  const obj = {};
  obj['status'] = false;
  obj['statusMessage'] = 'Validation error';
  obj['statusCode'] = 'VALIDATION_ERROR';
  obj['errors'] = errors || {};
  return obj;
};


/**
 *@description Unknown Route Handler
 */
exports.unknownRouteHandler = (req, res) => {
  return res.status(400).json({
    status: false,
    statusMessage: '404 - Page Not found',
    statusCode: 'routeNotFound',
    response: {},
  });
};


/**
 *@description Modify the error format.
 */
exports.parseValidationErrors = function (errors) {
  const errorObject = {};
  errors.forEach((elem) => {
    errorObject[elem.field || 'default'] = elem.message;
  });
  return errorObject;
};
