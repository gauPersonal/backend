"use strict";

function createRes(data) {
  return {
    status: true,
    statusCode: data.statusCode || 'SUCCESS',
    statusMessage: data.statusMessage || '',
    response: data.response || {},
  };
}

module.exports = {
  createRes
}