const constant = require('./constants');
const winston = require('winston');
const moment = require('moment');

/* eslint-disable max-len */
const myFormat = winston.format.printf(({ level, message, label }) => {
    return `'Time' : ${moment().format('LLL')} | 'Label' : [${label}] | 'Lavel' : ${level} | 'Message' : ${message}`;
});


const logger = winston.createLogger({
    handleException: true,
    maxSize: 52428800,
    prettyPrint: true,
    format: winston.format.combine(
        winston.format.label({ label: 'NODE_BACKEND' }),
        winston.format.timestamp(),
        myFormat,
    ),
    transports: [
        // new winston.transports.Console({}),
        new winston.transports.File({
            filename: `${constant.PATH.LOG}${moment().format('YYYY_MM_DD')}.log`,
        }),
    ],
});

/** Return Logger instances */
module.exports = logger;
