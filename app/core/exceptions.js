module.exports = (exceptionType, error) => {
	let exception = new Error();
	switch (exceptionType) {
		case 'AUTHENTICATION_ERROR':

			exception["status"] = 401;
			exception["message"] = 'Authentication Failed';
			exception["statusCode"] = 'authenticationError';
			break;

		case 'MISSING_USER_TYPE':

			exception["status"] = 401;
			exception["message"] = 'User Type is Required in headers.';
			exception["statusCode"] = 'authenticationError';
			break;

		case 'INVALID_USER_TYPE':

			exception["status"] = 401;
			exception["message"] = 'User Type is not valid.';
			exception["statusCode"] = 'authenticationError';
			break;

		case 'INVALID_EMAIL_ID':

			exception["status"] = 401;
			exception["message"] = 'Email id is not valid.';
			exception["statusCode"] = 'authenticationError';
			break;

		case 'EMAIL_ALREADY_EXISTS':

			exception["status"] = 401;
			exception["message"] = 'Email id is already used by another user.';
			exception["statusCode"] = 'emailAlreadyExists';
			break;

		case 'INVALID_PASSWORD':

			exception["status"] = 401;
			exception["message"] = 'Password is not valid.';
			exception["statusCode"] = 'authenticationError';
			break;

		case 'TOKEN_GEN_ERROR':

			exception["status"] = 402;
			exception["message"] = 'Error in JWT token Generation.';
			exception["statusCode"] = 'tokenGenError';
			break;

		case 'TOKEN_NOT_FOUND':

			exception["status"] = 404;
			exception["message"] = 'JWT token is missing.';
			exception["statusCode"] = 'tokenNotFound';
			break;

		case 'MYSQL_DB_CONNECTION_ERROR':

			exception["status"] = 503;
			exception["message"] = 'Error accured in mysql db connection.';
			exception["statusCode"] = 'dbConnectionError';
			break;

		case 'MYSQL_QUERY_ERROR':

			exception["status"] = 503;
			exception["message"] = error.message || 'Error accured in mysql query.';
			exception["statusCode"] = 'mysqlQueryError';
			break;

		case 'FILE_NOT_FOUND':

			exception["status"] = 404;
			exception["message"] = 'File not found.';
			exception["statusCode"] = 'fileNotFound';
			break;

		case 'FILE_TYPE_NOT_ALLOWED':

			exception["status"] = 405;
			exception["message"] = 'File type not allowed';
			exception["statusCode"] = 'fileTypeNotAllowed';
			break;


		case "FILE_SIZE_NOT_ALLOWED":

			exception["status"] = 405;
			exception["message"] = `This file can't be uploaded(Maximum file size exceeded)`;
			exception["statusCode"] = 'fileSizeNotAllowed';
			break;

		case "MOBILE_ALREADY_REGISTERD":

			exception["status"] = 409;
			exception["message"] = `Mobile number already registerd`;
			exception["statusCode"] = 'mobileAlreadyRegisterd';
			break;

		case "MOBILE_NOT_FOUND":

			exception["status"] = 404;
			exception["message"] = `Mobile number not found`;
			exception["statusCode"] = 'mobileNotFound';
			break;


		default:

			exception["status"] = 401;
			exception["message"] = 'Unhandeled Error Accured';
			exception["statusCode"] = 'unhandeledError';

	}
	return exception;
};
